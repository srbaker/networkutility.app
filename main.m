/*
 * This file is part of NetworkUtility.
 *
 * NetworkUtility is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NetworkUtility is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NetworkUtility.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include <Renaissance/Renaissance.h>

/* Dummy function pointer needed to link Renaissance.dll on Windows.

   When this missing, also causes a weird error:

   Uncaught exception NSInvalidArgumentException, reason: NSBundle(class) does not recognize loadGSMarkupNamed:owner:

   Must be a GCC issue, not a Windows issue.
*/
int (*linkRenaissanceIn)(int, const char **) = GSMarkupApplicationMain;

#include "MainController.h"

int main (int argc, const char **argv, char** env)
{
  CREATE_AUTORELEASE_POOL (pool);

  [NSApplication sharedApplication];
  [NSApp setDelegate: [MainController new]];

  [NSBundle loadGSMarkupNamed: @"MainMenu"  owner: [NSApp delegate]];

  [NSApp run];
  RELEASE (pool);
    
  return 0;
}
