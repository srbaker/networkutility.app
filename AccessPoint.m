/*
 * This file is part of NetworkUtility.
 *
 * NetworkUtility is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NetworkUtility is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NetworkUtility.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "AccessPoint.h"

@implementation AccessPoint

-(NSString *)name
{
  return name;
}

-(void)name: (NSString *)newName
{
  name = newName;
}

-(BOOL)connected
{
  return connected;
}

-(void)connected: (BOOL)isConnected
{
  connected = isConnected;
}

+(NSArray *)getAccessPoints
{
  id accessPointList = [[NSMutableArray alloc] init];

  NSTask *task = [[NSTask alloc] init];
  NSPipe *pipe = [NSPipe pipe];
  NSFileHandle *readHandle = [pipe fileHandleForReading];
  NSData *inData = nil;

  [task setStandardOutput: pipe];
  [task setLaunchPath: @"/usr/bin/nmcli"];
  [task setArguments: [NSArray arrayWithObjects: @"-t", @"device", @"wifi", @"list", nil]];
  [task launch];
  [task waitUntilExit];

  NSMutableData *outputData = [[NSMutableData alloc] init];
  while ((inData = [readHandle availableData]) && [inData length])
    {
      [outputData appendData: inData];
    }

  NSString *dataString = [[NSString alloc] initWithData: outputData encoding: NSUTF8StringEncoding];
  NSString *escapedDataString = [dataString stringByReplacingOccurrencesOfString: @"\\:" withString: @""];

  for (NSString *line in [escapedDataString componentsSeparatedByString: @"\n"])
    {
      NSArray *components = [line componentsSeparatedByString:@":"];

      if ([components count] > 2)
        {
          if ([[components objectAtIndex: 2] isEqual: @""])
            {
              continue;
            }

          id accessPoint = [[AccessPoint alloc] init];
          [accessPoint name: [[components objectAtIndex: 2] copy]];
          if ([[components objectAtIndex: 0] isEqual: @"*"])
            {
              [accessPoint connected: YES];
            }
          [accessPointList addObject: accessPoint];
        }
    }
  [outputData release];
  [task release];

  return accessPointList;
}

@end
